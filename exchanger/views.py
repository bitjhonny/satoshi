import uuid

from annoying.decorators import render_to
from django.http import Http404
from django.shortcuts import redirect

from exchanger.forms import ProtectedFileForm
from exchanger.models import ProtectedFile


@render_to('index.html')
def show_index_page(request):
    if request.method == 'POST':
        form = ProtectedFileForm(request.POST, request.FILES)
        if form.is_valid():
            form.instance.url_hash = str(uuid.uuid4())
            f = form.save()
            return redirect('file', url_hash=f.url_hash)

        return {'form': form}

    return {'form': ProtectedFileForm}


@render_to('overview.html')
def file_view(request, url_hash):
    try:
        f = ProtectedFile.objects.get(url_hash=url_hash)
    except ProtectedFile.DoesNotExist:
        raise Http404
    return {'f': f}
