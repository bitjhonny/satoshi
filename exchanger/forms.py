from django.forms import ModelForm

from .models import ProtectedFile


class ProtectedFileForm(ModelForm):
    class Meta:
        model = ProtectedFile
        fields = ['cost', 'bitcoin_address', 'file']
