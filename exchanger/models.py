from django.db import models

DEFAULT_MAX_LENGTH = 255
BITCOIN_MAX_LENGTH = 35


class ProtectedFile(models.Model):
    url_hash = models.CharField(blank=True, unique=True, max_length=DEFAULT_MAX_LENGTH)
    cost = models.DecimalField(max_digits=30, decimal_places=8)
    bitcoin_address = models.CharField(max_length=BITCOIN_MAX_LENGTH)
    file = models.FileField(upload_to='files')


class Transaction(models.Model):
    address_from = models.CharField(max_length=BITCOIN_MAX_LENGTH)
    how_much = models.DecimalField(max_digits=30, decimal_places=8)
    dtm = models.DateTimeField(auto_now_add=True)
