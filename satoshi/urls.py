from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin

from exchanger import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.show_index_page, name='index'),
    url(r'^files/(?P<url_hash>[\d\w-]+)/', views.file_view, name='file')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)